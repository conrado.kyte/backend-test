# Backend Service

This project is a simple FastAPI application with a `/health` endpoint, designed to demonstrate the creation, containerization, and deployment of a Python web service. It uses Poetry for dependency management to ensure a reproducible environment.

## Features

- **FastAPI**: An efficient web framework for building APIs with Python 3.11.
- **Poetry**: Dependency management and packaging made easy.
- **Docker**: Containerization for easy deployment and scaling.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Python 3.11
- [Poetry 1.7](https://python-poetry.org/docs/#installation)
- Docker

### Installing

Install the dependencies using Poetry:

```bash
poetry install
```

### Running the Application

#### To run the application locally (outside Docker):

```bash
poetry run uvicorn main:app --reload
```

The application will be available at http://127.0.0.1:8000.

#### Running with Docker

To build the Docker image:

```bash
docker build -t backend-service .
````

To run the Docker container:

```bash
docker run -d --name backend-service -p 8000:8000 backend-service
```

The application will now be accessible at http://127.0.0.1:8000.

### Testing
To test the /health endpoint, you can use curl or navigate to http://127.0.0.1:8000/health in your web browser. You should receive a response indicating that the service is running:

```bash
curl http://127.0.0.1:8000/health
````

Response:

```json
{"status": "running"}
```

## Deployment

Export env variables:

```bash
export DO_TOKEN=<DIGITAL_OCEAN_TOKEN>
export TF_VAR_do_token=$DO_TOKEN
````

Create the infrastructure:

```bash
cd ops/terraform
terraform init
terraform plan
terraform apply
```

Build the docker image

```bash
docker build -t backend-service .
```

Login the registry

```bash 
export $DO_TOKEN=<DIGITAL_OCEAN_TOKEN>
docker login -u $DO_TOKEN -p $DO_TOKEN registry.digitalocean.com/test-backend-registry
```

Push the image to the registry

```bash
docker tag backend-service registry.digitalocean.com/test-backend-registry/backend-service
docker push registry.digitalocean.com/test-backend-registry/backend-service
```

Deploy the application and service:

```bash
kubectl apply -f ops/kubernetes/backend_deployment.yml
kubectl apply -f ops/kubernetes/backend_service.yaml
```
