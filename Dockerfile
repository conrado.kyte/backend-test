FROM python:3.11-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV POETRY_VERSION 1.7.1
ENV POETRY_VIRTUALENVS_CREATE=false

RUN pip install --upgrade pip && \
    pip install "poetry==$POETRY_VERSION" 

WORKDIR /app

COPY pyproject.toml poetry.lock* /app/

RUN poetry install --no-dev --no-interaction --no-ansi

COPY . /app

CMD ["poetry", "run", "uvicorn", "main:app", "--host=0.0.0.0"]
