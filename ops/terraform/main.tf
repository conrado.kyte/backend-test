resource "digitalocean_container_registry" "backend_registry" {
  name                    = "test-backend-registry"
  subscription_tier_slug  = "starter"
}

resource "digitalocean_kubernetes_cluster" "backend_cluster" {
  name    = "backend-cluster"
  region  = "nyc1"
  version = "1.29.1-do.0"

  node_pool {
    name       = "default"
    size       = "s-2vcpu-2gb"
    node_count = 1
  }
}
