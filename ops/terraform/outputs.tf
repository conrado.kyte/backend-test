output "kubeconfig" {
  sensitive = true
  value     = digitalocean_kubernetes_cluster.backend_cluster.kube_config.0.raw_config
}

output "cluster_endpoint" {
  value     = digitalocean_kubernetes_cluster.backend_cluster.endpoint
}

output "registry_name" {
  value = digitalocean_container_registry.backend_registry.name
}

output "registry_endpoint" {
  value = digitalocean_container_registry.backend_registry.endpoint
}
